# OpenFlexure Microscope Commissioning

Once you have built an OpenFlexure Microscope, this script aims to help you verify it works correctly.  It is very much a work in progress at present.

To run it, it's best to use a Jupyter notebook running on the Raspberry Pi embedded in your microscope.  There is already a Python environment installed with this available, on the latest (May 2022) OpenFlexure Raspbian SD card image.  For simplicity, you can set up the necessary extra packages and run the notebook using the included shellscript.

Currently, the best way to install and run this is to log in (with SSH, or a terminal) and type:

```console
cd
git clone https://gitlab.com/openflexure/microscope-commissioning-script.git
cd microscope-commissioning-script
./run_commissioning_notebook.sh
```

This will start a Jupyter notebook.  Navigate to `microscope.local:8888` and select `Commissioning.ipynb` to get started.  You should run each cell in turn, following the instructions as appropriate.