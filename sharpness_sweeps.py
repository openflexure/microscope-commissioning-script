import numpy as np
import openflexure_microscope_client
import time
import json
import sys
import logging

def unpack(scan_data, start_index = 0):
    jpeg_times = scan_data['jpeg_times']
    jpeg_sizes = scan_data['jpeg_sizes']
    jpeg_sizes_MB = [x / 10**3 for x in jpeg_sizes]
    stage_times = scan_data['stage_times']
    stage_positions = scan_data['stage_positions']
    stage_height = [pos[2] for pos in stage_positions]

    jpeg_heights = np.interp(jpeg_times,stage_times,stage_height)

    return jpeg_heights[start_index:], jpeg_sizes_MB[start_index:]

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def perform_sweeps(microscope, dzs = [20, 50, 100, 200]):
    # Get the data from an autofocus and do some really messy slicing
    # Only want the data for the main sweep up and down the range, clearly split
    microscope.autofocus()
    m = microscope.autofocus()

    peak_z = microscope.position['z']

    time_limits = [m['stage_times'][2], m['stage_times'][6]]
    jpeg_time_mask = np.where(np.logical_and(np.asarray(m['jpeg_times']) > time_limits[0], np.asarray(m['jpeg_times']) < time_limits[1]))
    heights, sizes = unpack(m)

    heights = np.asarray(heights)[jpeg_time_mask]
    sizes = np.asarray(sizes)[jpeg_time_mask]
    heights_up = heights[0:int(len(heights) / 2)]
    sizes_up = sizes[0:int(len(sizes) / 2)]
    up_peak_height = heights_up[np.argmax(sizes_up)]

    heights_down = heights[int(len(heights) / 2):]
    sizes_down = sizes[int(len(sizes) / 2):]
    down_peak_height = heights_down[np.argmax(sizes_down)]

    peak_mean = (down_peak_height + up_peak_height) / 2

    discrete_scans = {}

    # Move up and down in discrete steps, recording the image size at each point.
    # Settling time is 0.3, to minimise effect of vibrations on measurement

    for step_sizes in dzs:
        logging.info('Current dz is {0}'.format(step_sizes))
        start = time.time()

        if step_sizes >= 100:
            zrange = 2000
        else:
            zrange = 800

        microscope.move_rel((0,0, (peak_mean - zrange / 2) - microscope.position['z']))

        discrete_scans[step_sizes] = {}
        discrete_scans[step_sizes]['up'] = {}
        discrete_scans[step_sizes]['down'] = {}
        
        while microscope.position['z'] <= (peak_mean + zrange / 2):
            s = microscope.grab_image_size() / 10**3
            h = microscope.position['z']
            discrete_scans[step_sizes]['up'][h] = s
            microscope.move_rel((0,0,step_sizes))
            time.sleep(0.3)

        while microscope.position['z'] >= (peak_mean - zrange / 2):
            s = microscope.grab_image_size() / 10**3
            h = microscope.position['z']
            discrete_scans[step_sizes]['down'][h] = s
            microscope.move_rel((0,0,-step_sizes))
            time.sleep(0.3)

        logging.info("Sweep took {}s".format(round(time.time()-start, 0)))

    microscope.move_rel((0,0,peak_mean - microscope.position['z']))

    # More ugly splitting for the continuous scan into dictionary
    # Ends with one dict called 'all_moves', containing all heights and sizes for the different moves

    heights = {}
    heights['up'] = {}
    heights['down'] = {}

    for i in range(len(heights_up)):
        heights['up'][heights_up[i]] = sizes_up[i]

    for i in range(len(heights_up)):
        heights['down'][heights_down[i]] = sizes_down[i]
        
    all_moves = {}
    all_moves['continuous'] = heights
    all_moves['discrete'] = discrete_scans

    return all_moves

   

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    ip = str(sys.argv[1])
    microscope = openflexure_microscope_client.MicroscopeClient(ip)
    try:
        dzs = sys.argv[2].split(",")
        dzs = list(map(int, dzs))
    except:
        dzs = [20, 50, 100, 200]
        pass
    moves = perform_sweeps(microscope, dzs)

    # Save dict as a JSON. Requires np.array conversion to lists

    with open('data_nb.json', 'w') as fp:
        json.dump(all_moves, fp,  indent=4, cls=NumpyEncoder)