#!/bin/bash

# Fix permissions
sudo chown -R pi ~pi/microscope_python_scripts
sudo chmod -R g+w /var/openflexure/extensions

# Activate client Python environment and install dependencies
source ~pi/microscope_python_scripts/.venv/bin/activate
pip install camera-stage-mapping matplotlib

# Set a jupyter password
jupyter notebook password
jupyter notebook . --ip="*"